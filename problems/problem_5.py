"""
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
"""


# The idea would be to find the factor decomposition of all the number below or equal to 20. then
#  take the overlapping if the decomposition (e.g. 8 is 2*2*2 and 18 is 3*3*2 -> 2*2*3*3 will
#  be evenly divided by 8 and 18
# 2*3*5*7*11*13*17*19*2*2*3*2
def factor_decomposition(number):
    pass

def is_prime(number):
    number = int(number)
    half = int(number / 2)
    for n in range(2, half):
        if number % n == 0:
            return False
    return True

prime_under_20 = [i for i in range(2,21) if is_prime(i)]

print(prime_under_20)
min = prime_under_20[0]
for prime in prime_under_20[1:]:
    min = min * prime

max = min * 2

n_under_20 = list(range(1, 21))
print(min, max, n_under_20)
for i in range(min, max+1):
    # print(i)
    # print([i % n == 0 for n in n_under_20])
    if all([i % n == 0 for n in n_under_20]):
        print(i)
