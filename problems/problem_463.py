"""The function ff is defined for all positive integers as follows:

f(1)=1
f(3)=3
f(2n)=f(n)
f(4n+1)=2f(2n+1)−f(n)
f(4n+3)=3f(2n+1)−2f(n)
The function S(n) is defined as ∑i=1n f(i).

S(8)=22 and S(100)=3604.

Find S(3^37. Give the last 9 digits of your answer."""


def get_new_base_function_dict():
    return {1: 1, 3: 3}

def f(n, function_dict):
    if n not in function_dict:
        if (n % 2) == 0:
            function_dict[n] = f(int(n/2), function_dict)
        elif (n-1) % 4 == 0:
            n_temp = int((n-1) / 4)
            function_dict[n] = 2 * f(2 * n_temp + 1, function_dict) - f(n_temp, function_dict)
        elif (n-3) % 4 == 0:
            n_temp = int((n - 3) / 4)
            function_dict[n] = 3 * f(2 * n_temp + 1, function_dict) - 2 * f(n_temp, function_dict)
    return function_dict.get(n, None)



f_dict = get_new_base_function_dict()

print(f(2, f_dict))
print(f(4, f_dict))
print(f(8, f_dict))
print(f(5, f_dict))
print(f(6, f_dict))
print(f(7, f_dict))

def sum_f(n):
    s_dict = get_new_base_function_dict()
    for i in range(1, n+1):
        f(i, s_dict)
    return sum(s_dict.values())

print(sum_f(8))
print(sum_f(100))

# Way too long...
# print(sum_f(int(3 ** 37)))






