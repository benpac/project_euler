"""
 For fixed integers a, b, c, define the crazy function F(n) as follows:
F(n) = n - c for all n > b
F(n) = F(a + F(a + F(a + F(a + n)))) for all n ≤ b.

Also, define S(a,b,c)=∑_n=0^b F(n)

.

For example, if a = 50, b = 2000 and c = 40, then F(0) = 3240 and F(2000) = 2040.
Also, S(50, 2000, 40) = 5204240.

Find the last 9 digits of S(21^7, 7^21, 12^7).
"""


abc = (50, 2000, 40)


def F(n, a, b, c, F_dict):
    if n in F_dict:
        return F_dict[n]
    if n > b:
        F_dict[n] = n - c
    else:
        args = (a, b, c, F_dict)
        value = F(a + F(a + F(a + F(a + n, *args), *args), *args), *args)
        F_dict[n] = value
    return F_dict[n]


F_dict_50_2000_40 = {}

print(F(0, *abc, F_dict_50_2000_40))
print(F(2000, *abc, F_dict_50_2000_40))

F_dict_big = {}

abc = (21 ** 7, 7 ** 21, 12 ** 7)

# MEMORY ERROR... Dommage :(

sum = 0
for n in reversed(range(0, abc[1] + 1)):
    sum += F(n, *abc, F_dict_big)

print(sum)
