"""
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
"""
import math

def is_prime(number, primes=None):
    number = int(number)
    limit = int(math.sqrt(number))
    list = range(2, limit)
    if primes is not None and len(primes) > 0:
        list = primes
    for n in list:
        if number % n == 0:
            return False
    return True

nth_index = 10001

limit = 10000 * 100

# def sieve(list, nth):
#     number = list[nth]
#     max = list[-1]
#     print(number, max)
#     multiples = [number * i for i in range(2, int(max/number)+1)]
#     new_list = [n for n in list if n not in multiples]
#     return new_list
#
# prime_list = list(range(1, limit, 2))
# print(prime_list)
#
# for sieve_idx in range(1, nth_index):
#     print(sieve_idx)
#     prime_list = sieve(prime_list, sieve_idx)

prime_list = []
for i in range(2, limit):
    if is_prime(i, prime_list):
        print(i)
        prime_list.append(i)

assert len(prime_list) >= nth_index


print(prime_list[nth_index-1])

print(prime_list)

print(len(prime_list))

print(prime_list[nth_index])

assert is_prime(prime_list[nth_index-1])
