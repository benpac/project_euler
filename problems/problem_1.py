"""
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
"""


max = 1000


def all_mult_of_below(mult_base, max):
    all_mult = [mult_base * i for i in range(int(max/mult_base)+1) if mult_base * i < max]
    print(all_mult)
    return all_mult


multiples = [3, 5]

all_numbers = set()
for mult in multiples:
    all_numbers.update(set(all_mult_of_below(mult, max)))
print(all_numbers)
total = sum(all_numbers)

print("The sum of all multiple of {} below {} is {}".format(
    " and ".join([str(m) for m in multiples]), max, total))
