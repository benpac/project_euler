"""
By starting at the top of the triangle below and moving to adjacent numbers on the row below,
the maximum total from top to bottom is 23.

3
7 4
2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom in triangle.txt (right click and 'Save Link/Target As...'),
 a 15K text file containing a triangle with one-hundred rows.

NOTE: This is a much more difficult version of Problem 18. It is not possible to try every route to
solve this problem, as there are 299 altogether! If you could check one trillion (1012) routes every
second it would take over twenty billion years to check them all. There is an efficient algorithm to
solve it. ;o)
"""

def read_file(filename='p067_triangle.txt'):
    _triangle = []
    with open(filename) as f_tri:
        lines = f_tri.readlines()
    for line in lines:
        _triangle.append([int(i) for i in line.split()])
    return _triangle


triangle = read_file()

# Going bottom up, we update the next layer up with the highest of the two below.

layer_index = len(triangle)-2

print(layer_index, len(triangle))

while layer_index >= 0:
    print(layer_index)
    bottom_layer = triangle[layer_index+1]
    layer = triangle[layer_index]
    new_layer = []
    for index in range(len(layer)):
        new_layer.append(layer[index] + max(bottom_layer[index], bottom_layer[index+1]))
    triangle[layer_index] = new_layer
    layer_index -= 1

print(triangle[0][0])

