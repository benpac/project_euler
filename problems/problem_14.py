"""The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
"""

import operator

previous_collatz_chains = {1: 1}

def compute_collatz(start_value):
    index = 1
    n = start_value
    while n != 1:
        if n in previous_collatz_chains:
            index += previous_collatz_chains[n]-1
            break
        index += 1
        if n % 2 == 0:
            n = n/2
        else:
            n = 3*n +1
    previous_collatz_chains[start_value] = index


compute_collatz(1)
print(previous_collatz_chains)
compute_collatz(2)
print(previous_collatz_chains)
compute_collatz(8)
print(previous_collatz_chains)
compute_collatz(3)
print(previous_collatz_chains)


for n in range(1, int(1e6)):
    compute_collatz(n)

max_chain = max(previous_collatz_chains.items(), key=operator.itemgetter(1))
print(max_chain)

# print(previous_collatz_chains)

