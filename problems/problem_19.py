"""You are given the following information, but you may prefer to do some research for yourself.

1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.
A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?"""


def is_sunday(days_from_1_jan_1900):
    return days_from_1_jan_1900 % 7 == 0


def month_length(year, month_index):
    if month_index in [1, 3, 5, 7, 8, 10, 12]:
        return 31
    elif month_index in [4, 6, 9, 11]:
        return 30
    else:
        is_leap = False
        if year % 4 == 0:
            if year % 100 == 0:
                if year % 400 == 0:
                    is_leap = True
            else:
                is_leap = True
        if is_leap:
            return 29
        else:
            return 28


def get_1_jan_1901():
    days = 1
    for i in range(1, 13):
        days += month_length(1901, i)
    return days

sundays_on_first = []

first_jan_1901 = get_1_jan_1901()
print(first_jan_1901)
considered_day = first_jan_1901
if is_sunday(first_jan_1901):
    sundays_on_first.append(first_jan_1901)
for year in range(1901, 2001):
    for month in range(1, 13):
        considered_day += month_length(year, month)
        if is_sunday(considered_day):
            sundays_on_first.append(considered_day)


print(sundays_on_first)

print(len(sundays_on_first))
