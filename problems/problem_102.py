"""
Three distinct points are plotted at random on a Cartesian plane, for which -1000 ≤ x, y ≤ 1000,
such that a triangle is formed.

Consider the following two triangles:

A(-340,495), B(-153,-910), C(835,-947)

X(-175,41), Y(-421,-714), Z(574,-645)

It can be verified that triangle ABC contains the origin, whereas triangle XYZ does not.

Using triangles.txt (right click and 'Save Link/Target As...'), a 27K text file containing the
co-ordinates of one thousand "random" triangles, find the number of triangles for which the interior
contains the origin.

NOTE: The first two examples in the file represent the triangles in the example given above.
"""
import numpy as np

# TODO: Seems incorrect


with open("p102_triangles.txt") as f:
    all_tri_lines = f.readlines()

all_tri = []
for line in all_tri_lines:
    coords = [int(c) for c in line.split(',')]
    all_tri.append(coords)

# we will solve the number of intersection of the vectors with the x and y axis (conjecture)
# For the vector AB = (u, v) where u and v are x/y_b - x/y_a, we have an intersection with the x
# axis if (x_a, y_a) + k * (u, v) = (x, 0) -> x = x_a + -(y_a/v) * u. This is on the segment if
# 1 >= -(y_a/v) >= 0

n_tri = 0
index = 0
for tri in all_tri:
    index += 1
    a, b, c = np.array(tri[:2]), np.array(tri[2:4]), np.array(tri[4:])
    print(f"Triangle {index}: \n{a}\t{b}\t{c}")
    ab = b - a
    bc = c - b
    ca = a - c
    intersections_with_x = []
    intersections_with_y = []
    for point, vector in zip((a, b, c), (ab, bc, ca)):
        k_x = -point[1]/vector[1]
        k_y = -point[0]/vector[0]
        print(f"point {point}, vector {vector}, k's {k_x} {k_y}")
        if 1 >= k_x >= 0:
            intersections_with_x.append(point[0] + k_x * vector[0])
        if 1 >= k_y >= 0:
            intersections_with_y.append(point[1] + k_y * vector[1])

    print(f"Got intersections x: {intersections_with_x}, intersection y: {intersections_with_y}")

    if len(intersections_with_x) == 2 and len(intersections_with_y) == 2:
        if intersections_with_x[0] * intersections_with_x[1] <= 0 and \
                intersections_with_y[0] * intersections_with_y[1] <= 0:
            print(f"Triangle {index} has origin in the interior")
            n_tri += 1


print("Number of triangles with the origine in the interior:")
print(n_tri)
