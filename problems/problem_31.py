"""
In England the currency is made up of pound, £, and pence, p, and there are eight coins in
general circulation:

1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
It is possible to make £2 in the following way:

1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
How many different ways can £2 be made using any number of coins?
"""

from operator import itemgetter

coin_values = {
    "1P": 100,
    "50p": 50,
    "20p": 20,
    "10p": 10,
    "5p": 5,
    "2p": 2,
    "1p": 1
}

n_ways = 1  # 1x2P

goal = 200

sorted_coins_names = [a[0] for a in sorted(coin_values.items(), key=itemgetter(1), reverse=True)]


def add_coin_if_possible(coins, max_val_name):
    current_total = count_coins(coins)
    max_value = coin_values[max_val_name]
    if (current_total + max_value) <= goal:
        coins.append(max_val_name)
        return True
    else:
        return False


def get_lower_value_name(value_name):
    value = coin_values[value_name]
    for coin_name in sorted_coins_names:
        if coin_values[coin_name] < value:
            return coin_name
    return None


def count_coins(coins):
    total = sum([coin_values[name] for name in coins])
    return total


for coin_name in sorted_coins_names:
    test_coin_value = coin_name
    while test_coin_value is not None:
        coin_list = [test_coin_value]
        while add_coin_if_possible(coin_list, test_coin_value):
            if count_coins(coin_list) == goal:
                n_ways += 1
            break
        test_coin_value = get_lower_value_name(test_coin_value)

        print(coin_list)


print(sorted_coins_names)
