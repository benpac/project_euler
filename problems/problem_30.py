"""Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:

1634 = 14 + 64 + 34 + 44
8208 = 84 + 24 + 04 + 84
9474 = 94 + 44 + 74 + 44
As 1 = 14 is not a sum it is not included.

The sum of these numbers is 1634 + 8208 + 9474 = 19316.

Find the sum of all the numbers that can be written as the sum of fifth powers of their digits."""


def get_digits(number):
    digits = [int(s) for s in str(number)]
    return digits


def get_sum_n_power(digits, n):
    sum_v = sum([d ** n for d in digits])
    return sum_v


def is_sum_of_digit_to_n_power(number, n):
    sum_v = get_sum_n_power(get_digits(number), n)
    return sum_v == number


power = 5


numbers = []
for i in range(10, int(1e7)):
    if is_sum_of_digit_to_n_power(i, power):
        numbers.append(i)

print(numbers)
print(sum(numbers))

# answer:
# [4150, 4151, 54748, 92727, 93084, 194979]
# 443839