#
#
# We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example,
# the 5-digit number, 15234, is 1 through 5 pandigital.
#
# The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product
# is 1 through 9 pandigital.
#
# Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9
# pandigital.
# HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.


digits = [1, 2, 3, 4, 5, 6, 7, 8, 9]


def permutation(initial_list):
    if len(initial_list) == 0:
        return []
    if len(initial_list) == 1:
        return [initial_list]

    sub_list = []
    for i in range(len(initial_list)):
        start = initial_list[i]

        remaining = initial_list[:i] + initial_list[i + 1:]
        for p in permutation(remaining):
            sub_list.append([start] + p)
    return sub_list


def generate_splits(base_list):
    _splits = []
    for i in range(1, len(base_list) - 1):
        for j in range(i+1,  len(base_list)-1):
            _splits.append([base_list[:i], base_list[i:j], base_list[j:]])
    return _splits


def compute_and_check_products(first, mid, end):
    first_num = create_number_from_list(first)
    mid_num = create_number_from_list(mid)
    end_num = create_number_from_list(end)
    result = []
    if first_num * mid_num == end_num:
        result.append(end_num)
        # print(f'{first_num} * {mid_num} = {end_num}')
    if first_num * end_num == mid_num:
        result.append(mid_num)
        # print(f'{first_num} * {end_num} = {mid_num}')
    if mid_num * end_num == first_num:
        result.append(first_num)
        # print(f'{mid_num} * {end_num} = {first_num}')
    return result


def create_number_from_list(numbers):
    return int("".join([f'{i}' for i in numbers]))


permutations = permutation(digits)
all_product = set()
for perm in permutations:
    splits = generate_splits(perm)
    for split in splits:
        all_product.update(set(compute_and_check_products(*split)))
# print(all_product)
print(f'The sum of all products is {sum(all_product)}')



