"""Start from an ordered list of all integers from 1 to n. Going from left to right, remove the first number and every other number afterward until the end of the list. Repeat the procedure from right to left, removing the right most number and every other number from the numbers left. Continue removing every other numbers, alternating left to right and right to left, until a single number remains.

Starting with n = 9, we have:
1 2 3 4 5 6 7 8 9
2 4 6 8
2 6
6

Let P(n) be the last number left starting with a list of length n.
Let S(n)=∑k=1nP(k).
You are given P(1)=1, P(9) = 6, P(1000)=510, S(1000)=268271.

Find S(1018) mod 987654321.
"""

# Brute force is not working... Need someting else

def p(n):
    p_list = list(range(1, n+1))
    left = True
    # print(p_list)
    while len(p_list) > 1:
        # print(p_list)
        if left:
            # print('left')
            p_list = p_list[1::2]
            left = False
        else:
            # print('right')
            p_list = p_list[-2::-2]
            p_list.reverse()
            left = True
    return p_list[0]


def s(n, step):
    tot = 0
    for k in range(1, n+1):
        if k % step == 0:
            print(k)
        # print(k)
        tot += p(k)
    return tot


print("P(1)", p(1))
print("P(9)", p(9))
print("P(1000)", p(1000))

a = s(1000, 10)

print("S(1000)", a)

# this is too long
# b = s(int(1e18), int(1e9))
# print(b % 987654321)

