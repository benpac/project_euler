"""
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""


def m(n1, n2):
    return n1 * n2


all_3digit_number = range(100, 1000)


def get_all_pairs(list_numbers):
    pairs = []
    for index, element in enumerate(list_numbers):
        pairs.extend([(element, list_numbers[index+i]) for i in range(len(list_numbers)-index)])
    return pairs


def is_palindrome(number):
    number_str = str(number)
    while len(number_str) > 1:
        if number_str[0] == number_str[-1]:
            number_str = number_str[1:-1]
        else:
            return False
    return True

palindromes = {}
last_palindrome = None
for pair in get_all_pairs(all_3digit_number):
    product = m(pair[0], pair[1])
    if is_palindrome(product):
        palindromes[pair] = product
        last_palindrome = product

print (last_palindrome)

print(palindromes)

print(max(palindromes.values()))

