#
#
# Take the number 192 and multiply it by each of 1, 2, and 3:
#
#     192 × 1 = 192
#     192 × 2 = 384
#     192 × 3 = 576
#
# By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated
# product of 192 and (1,2,3)
#
# The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 918273645,
# which is the concatenated product of 9 and (1,2,3,4,5).
#
# What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer
# with (1,2, ... , n) where n > 1?


def create_number(base_num, max_n):
    nums = []
    for i in range(1, max_n + 1):
        nums.append(i * base_num)
    return ''.join((str(_num) for _num in nums))


def check_pandigital(num_to_check: str):
    if len(num_to_check) != 9:
        return False
    return set([digit for digit in num_to_check]) == {'1', '2', '3', '4', '5', '6', '7', '8', '9'}


pandigital_numbers = set()
for i in range(1, 9999):
    for n in range(2, 9 // len(str(i)) + 1):
        num = create_number(i, n)
        if check_pandigital(num):
            pandigital_numbers.add(int(num))
            print(f'for i:{i}, (1, ..., {n}), we have the number :{num}')

print(sorted(pandigital_numbers))
print(max(pandigital_numbers))
