#
# A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits
# 1, 2, 3 and 4. If all the permutations are listed numerically or alphabetically, we call it lexicographic order.
# The lexicographic permutations of 0, 1 and 2 are:
#
# 012   021   102   120   201   210
#
# What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?


chars = '0,1,2,3,4,5,6,7,8,9'.split(',')


def permutation(l):
    if len(l) == 0:
        return []
    if len(l) == 1:
        return [l]

    sub_list = []
    for i in range(len(l)):
        start = l[i]

        remaining = l[:i] + l[i + 1:]
        for p in permutation(remaining):
            sub_list.append([start] + p)
    return sub_list


chars_permutations = permutation(chars)
concatenated = ["".join(perm) for perm in chars_permutations]
sorted(concatenated)
print(f'The 1000000th permutation is {concatenated[999999]}')
