"""
For a positive integer n, define f(n) as the least positive multiple of n that, written in
base 10, uses only digits ≤ 2.

Thus f(2)=2, f(3)=12, f(7)=21, f(42)=210, f(89)=1121222.


Also, ∑n_1^100 f(n)/n=11363107

Find ∑n=_1^10000 f(n)/n

"""

# TODO: Finish
# This is very slow for 999...
def f(n):
    m = n
    while not only_digit_smaller_equal_to(m, 2):
        m += n
    print(n, m)
    return m


def only_digit_smaller_equal_to(fn, m=2):
    return all([int(n) <= m for n in str(fn)])


end = 10000
sum = 0
for i in range(1, end+1):
    sum += (f(i) // i)

print(sum)