"""If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are
 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words,
how many letters would be used?


NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23
letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out
numbers is in compliance with British usage.
"""
import math

num_length = {
  0: 0,
  1: 3,
  2: 3,
  3: 5,
  4: 4,
  5: 4,
  6: 3,
  7: 5,
  8: 5,
  9: 4,
  10: 3,
  11: 6,
  12: 6,
  13: 8,
  14: 8,
  15: 7,
  16: 7,
  17: 9,
  18: 8,
  19: 8,
  20: 6,
  30: 6,
  40: 5,
  50: 5,
  60: 5,
  70: 7,
  80: 6,
  90: 6,
  100: 7,
  1000: 8,
  'and': 3
}

AND = 3


def get_tens(num):
    if 1 <= num <= 20:
        return [num]
    tens = int(num / 10)
    tens *= 10
    return [tens, num-tens]


def get_hundreds(num):
    assert 100<num<1000
    hundreds = int(num / 100)
    return [hundreds, 100]

def transform_to_individual(num):
    if num == 1000:
        return [1, 1000]
    if num == 100:
        return [1, 100]
    else:
        individuals = []
        if num < 100:
            individuals.extend(get_tens(num))
        else:
            hundreds = get_hundreds(num)
            n_hundred = hundreds[0] * hundreds[1]
            tens = get_tens(num-n_hundred)
            individuals.extend(hundreds)
            if tens[0] > 0:
                individuals.append('and')
            individuals.extend(tens)

        return individuals

def count_letters(individuals):
    print(individuals)
    print([num_length[v] for v in individuals])
    return sum([num_length[v] for v in individuals])

all_num = list(range(1, 1001))

total = 0
for num in all_num:
    letters = count_letters(transform_to_individual(num))
    print("final", num, letters)
    total += letters
print (total)
