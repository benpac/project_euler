"""By counting carefully it can be seen that a rectangular grid measuring 3 by 2
contains eighteen rectangles:


Although there exists no rectangular grid that contains exactly two million rectangles,
find the area of the grid with the nearest solution."""


class Rectangle:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def num_rect_in_grid(self, x_grid, y_grid):
        assert self.x <= x_grid
        assert self.y <= y_grid
        return (x_grid - self.x + 1) * (y_grid - self.y + 1)

class Grid(Rectangle):
    def __init__(self, x, y):
        super().__init__(x, y)

    def count_all_rectangles(self):
        rectangles = []
        for x in range(1, self.x+1):
            for y in range(1, self.y+1):
                rectangles.append(Rectangle(x, y))

        tot = 0
        for rect in rectangles:
            tot += rect.num_rect_in_grid(self.x, self.y)
        return tot

limit = 100

all_scores = {}
print("starting")
for x in range(1, limit+1):
    for y in range(1, limit+1):
        all_scores[(x, y)] = Grid(x, y).count_all_rectangles()

# closest to 2 millions
two_million = 2e6

current_closest = None
current_distance = 2e6

for x_y, score in all_scores.items():
    tmp_diff = abs(two_million - score)
    if tmp_diff < current_distance:
        current_distance = tmp_diff
        current_closest = x_y


print(current_closest, current_distance, all_scores[current_closest])
print(current_closest[0] * current_closest[1])
