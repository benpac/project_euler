"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""
import math

number = 600851475143

prime_factors = set()

def is_prime(number):
    number = int(number)
    half = math.sqrt(number)
    for n in range(2, int(half)):
        if number % n == 0:
            return False

    return True


list_primes = [i for i in range(2, 100000) if is_prime(i)]

print(list_primes)

while not is_prime(number):
    print(number)
    factor = None
    for prime in list_primes:
        if number % prime == 0:
            factor = prime
            break
    if factor is None:
        print('not enough primes...')
        break
    print('found', factor)
    prime_factors.add(factor)
    number = number // factor

prime_factors.add(number)

print(max(prime_factors))
