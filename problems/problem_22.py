#
#
# Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names,
# begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this
# value by its alphabetical position in the list to obtain a name score.
#
# For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53,
# is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.
#
# What is the total of all the name scores in the file?

FILE = 'p022_names.txt'


def get_names_in_order():
    with open(FILE, 'r') as f:
        names = f.read()
    names = names.split(',')
    names = [(name.strip('"')).upper() for name in names]
    return sorted(names)


STEP = -ord('A') + 1


def compute_score(name: str):
    val = 0
    for char in name:
        val += ord(char) + STEP
    return val


all_names = get_names_in_order()
tot = 0

for idx, name in enumerate(all_names):
    tot += (idx+1) * compute_score(name)

print(f'Total score of the file is {tot}')