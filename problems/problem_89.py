"""For a number written in Roman numerals to be considered valid there are basic rules which must be followed. Even though the rules allow some numbers to be expressed in more than one way there is always a "best" way of writing a particular number.

For example, it would appear that there are at least six ways of writing the number sixteen:

IIIIIIIIIIIIIIII
VIIIIIIIIIII
VVIIIIII
XIIIIII
VVVI
XVI

However, according to the rules only XIIIIII and XVI are valid, and the last example is considered to be the most efficient, as it uses the least number of numerals.

The 11K text file, roman.txt (right click and 'Save Link/Target As...'), contains one thousand numbers written in valid, but not necessarily minimal, Roman numerals; see About... Roman Numerals for the definitive rules for this problem.

Find the number of characters saved by writing each of these in their minimal form.

Note: You can assume that all the Roman numerals in the file contain no more than four consecutive identical units.
"""

values_dict = {
    "I": 1,
    "V": 5,
    "X": 10,
    "L": 50,
    "C": 100,
    "D": 500,
    "M": 1000,
    "IV": 4,
    "IX": 9,
    "CD": 400,
    "CM": 900,
    "XL": 40,
    "XC": 90
}


def read_roman_num(string):
    individuals = [s for s in string]
    tot = 0
    index = len(string) - 1
    while index > 0:
        letter = individuals[index]
        prev_letter = individuals[index - 1]
        if prev_letter == "I" and letter in ["V", "X"] or \
                prev_letter == "X" and letter in ["L", "C"] or \
                prev_letter == "C" and letter in ["D", "M"]:
            tot += values_dict[prev_letter + letter]
            index -= 2
        else:
            tot += values_dict[letter]
            index -= 1
    if index == 0:
        tot += values_dict[individuals[index]]
    return tot


def write_roman_num(num):
    pass


print(read_roman_num("MMMMDCCCCXXXXVI"))
print(read_roman_num("MMMMCMXLVI"))




