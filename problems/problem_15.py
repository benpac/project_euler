"""
Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.

How many such routes are there through a 20×20 grid?
"""

N = 20

n_steps = 2*N

n_ways = 0


prev_fact = {}

def factorial(n):
    if n == 1:
        return 1
    else:
        value = factorial(n-1) * n
        prev_fact[n] = value
        return value

print(factorial(n_steps) / (factorial(N) * factorial(N)))

