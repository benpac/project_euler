"""
The number 7 is special, because 7 is 111 written in base 2, and 11 written in base 6
(i.e. 710 = 11_6 = 111_2). In other words, 7 is a repunit in at least two bases b > 1.

We shall call a positive integer with this property a strong repunit. It can be verified that there are 8 strong repunits below 50: {1,7,13,15,21,31,40,43}.
Furthermore, the sum of all strong repunits below 1000 equals 15864.

Find the sum of all strong repunits below 1012.
"""
import math

def is_repunit(num, base):
    max_power = int(math.log(num, base))
    # print('is_repunit', num, base, max_power)
    while num > base:
        if num % base != 1:
            return False
        num -= base ** max_power
        max_power -= 1
        # print('   ', num, max_power)
    return max_power == 0
    # test_unit = [1]
    # test_base = [1]
    # tmp_num = sum(map(lambda x, y: x*y, test_unit, test_base))
    # while tmp_num < num:
    #     test_unit.append(1)
    #     test_base.append(base ** (len(test_unit)-1))
    #     # print(test_unit, test_base)
    #     tmp_num = sum(map(lambda x, y: x*y, test_unit, test_base))
    #     if tmp_num == num:
    #         return True
    # return False


def is_strong_repunit(num):
    for base in range(2, num-1):
        repunit = is_repunit(num, base)
        if repunit:
            # print('base', base)
            return True
    return False

print(is_repunit(7, 2))
# print(is_repunit(1e12, 1e12-1))
#
print(is_strong_repunit(5))
print(is_strong_repunit(7))

# print(is_strong_repunit(8))
# print(is_strong_repunit(13))
# exit(0)
tot = 0
N = int(1e12)
for i in range(1, N):
    if is_strong_repunit(i):
        # print(i)
        tot += i

print(tot + 1)
