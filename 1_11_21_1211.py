"""
Find the Nth entry in the series:
1
11
21
1211
.
.
.
"""

def find_next(entry):
    """
    :param entry:
    :return:
    """
    entry_str = str(entry)
    elements = []
    n_sub_elements = 0
    prev_num = entry_str[0]
    n_sub_elements += 1
    for num in entry_str[1:]:
        if num != prev_num:
            elements.append(str(n_sub_elements))
            elements.append(prev_num)
            prev_num = num
            n_sub_elements = 1
        else:
            n_sub_elements += 1

    elements.append(str(n_sub_elements))
    elements.append(prev_num)
    return "".join(elements)


def sum_str_element(entry):
    entry = str(entry)
    def generator(number_str):
        for number in number_str:
            yield int(number)

    return sum(generator(entry))


# print(find_next(find_next(find_next('5214'))))
element = '1'
for i in range(100):
    # print(element)
    element = find_next(element)
    print(i, sum_str_element(element))
# print(element)
print(sum_str_element(element))

